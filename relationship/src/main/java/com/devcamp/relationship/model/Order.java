package com.devcamp.relationship.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;

@Entity
@Table(name = "orders")
public class Order {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

	@Temporal(TemporalType.TIMESTAMP)
	@CreatedDate
	@Column(name = "create_at", updatable = false)
    private Date createAt;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id", nullable = false)
    private Customer customer;
	
    @OneToOne(fetch = FetchType.LAZY,
		cascade = CascadeType.ALL, mappedBy = "order")
    private Payment payment;

	@ManyToMany(fetch = FetchType.LAZY,
		cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "order_products",
		joinColumns = {@JoinColumn(name = "order_id")},
		inverseJoinColumns = {@JoinColumn(name = "product_id")})
    private Set<Product> products;
	
	public Order() {
		super();
	}

	public Order(long id, Customer customer, Payment payment, Set<Product> products) {
		super();
		this.id = id;
		this.customer = customer;
		this.payment = payment;
		this.products = products;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Payment getPayment() {
		return payment;
	}

	public void setPayment(Payment payment) {
		this.payment = payment;
	}

	public Set<Product> getProducts() {
		return products;
	}

	public void setProducts(Set<Product> products) {
		this.products = products;
	}
}
